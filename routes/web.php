<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('get-suggestions',[UserController::class, 'index']);
Route::post('send-request', [UserController::class, 'sendRequest']);
Route::get('get-sent-request', [UserController::class, 'getSentRequests']);
Route::delete('remove-user-connection/{connectionId}', [UserController::class, 'removeUserConnection']);
Route::get('get-received-request', [UserController::class, 'getReceivedRequests']);
Route::post('accept-received-request/{connectionId}',[UserController::class, 'acceptReceivedRequest']);
Route::get('get-connections}',[UserController::class, 'getConnections']);

