<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserConnection;

class UserController extends Controller
{
    
    public function index(){        
        $data = [];
        $data = auth()->user()->getCount();
        // Get the IDs of the user's connections
        $connectionIds = auth()->user()->connections->pluck('id')->toArray();
        // Exclude the IDs of the user and their connections
        $excludeIds = array_merge([auth()->user()->id], $connectionIds);
        // Get connection suggestions who are not the user or their connections
        $data['suggestions'] = User::whereNotIn('id', $excludeIds)->paginate(10);
      
        return response()->json($data);
    }
    public function sendRequest(Request $request){
        $receiverId = $request->receiverId;
        $userConnection = new UserConnection();
        $userConnection->sender_id = auth()->user()->id;
        $userConnection->receiver_id = $receiverId;
        $userConnection->save();

        $data = [];
        $data = auth()->user()->getCount();
        
        return response()->json($data);
    }
    public function getSentRequests(){
        $data = [];
        $data = auth()->user()->getCount();
        $user = auth()->user();
        $data['sentRequests'] = $user->sentRequests()->with('receiver')->paginate(10);

        return response()->json($data);

    }
    public function getReceivedRequests(){
        $data = [];
        $data = auth()->user()->getCount();
        $user = auth()->user();
        $data['receivedRequests'] = $user->receivedRequests()->with('sender')->paginate(10);

        return response()->json($data);

    }
    public function removeUserConnection($connectionId){
        
        $userConnection = UserConnection::find($connectionId);
        if(!empty($userConnection)){
            $userConnection->delete();
        }

        $data = auth()->user()->getCount();

        return response()->json($data);
    }
    public function acceptReceivedRequest($connectionId){
        
        $data = auth()->user()->getCount();

        $userConnection = UserConnection::find($connectionId);
        if(!empty($userConnection)){
            $userConnection->status = 1;
            $userConnection->save();
        }
        return response()->json($data);
    }
    public function getConnections(){
        $data = [];
        $data = auth()->user()->getCount();
        $user = auth()->user();
        $data['connections'] = $user->userConnections()->with('sender')->paginate(10);

        return response()->json($data);
    }
}
