<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['suggestions'] = auth()->user()->connectionSuggestions();
        $data['sentRequestsCount'] = auth()->user()->sentRequestsCount();
        $data['receivedRequestsCount'] = auth()->user()->receivedRequestsCount();
        $data['userConnectionsCount'] = auth()->user()->userConnectionsCount();
            
        return view('home', compact('data'));

    }
}
