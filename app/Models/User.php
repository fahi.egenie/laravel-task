<?php

namespace App\Models;

use Laravel\Sanctum\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // public $withCount = ['sentdRequests', 'receivedRequests'];


    public function sentRequests()
    {
        return $this->hasMany(UserConnection::class, 'sender_id')->where('status', 0);
    }

    public function receivedRequests()
    {
        return $this->hasMany(UserConnection::class, 'receiver_id')->where('status', 0);
    }

    public function acceptedRequests()
    {
        return $this->hasMany(UserConnection::class, 'sender_id')->where('status', 1);
    }

    public function userConnections()
    {
        return $this->belongsToMany(User::class, 'user_connections', 'sender_id', 'receiver_id')->where('status', 1);
    }

    public function connections()
    {
        return $this->belongsToMany(User::class, 'user_connections', 'sender_id', 'receiver_id');
    }
    public function userConnectionsCount(){
        return $this->userConnections()->count();
    }
    public function sentRequestsCount()
    {
        return $this->sentRequests()->count();
    }
    public function receivedRequestsCount()
    {
        return $this->receivedRequests()->count();
    }

    public function connectionSuggestions()
    {
        // Get the IDs of the user's connections
        $connectionIds = $this->connections->pluck('id')->toArray();
        // Exclude the IDs of the user and their connections
        $excludeIds = array_merge([$this->id], $connectionIds);
        // Get connection suggestions who are not the user or their connections
        return User::whereNotIn('id', $excludeIds)->get();
    }

    public function getCount(){
        $data['suggestionsCount'] = $this->connectionSuggestions()->count();
        $data['sentRequestsCount'] = $this->sentRequestsCount();
        $data['receivedRequestsCount'] = $this->receivedRequestsCount();
        $data['userConnectionsCount'] = $this->userConnectionsCount();
        return $data;
    }
}
