var skeletonId = 'skeleton';
var contentId = 'content';
var skipCounter = 0;
var takeAmount = 10;
var page = 1;
var suggestionsList = '';
var sentRequestList = '';
var receivedRequestList = '';
var connectionsList = '';

$.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  })



function getRequests(mode) {
  // your code here...
}

function getMoreRequests(mode) {
  // Optional: Depends on how you handle the "Load more"-Functionality
  // your code here...
}

function getConnections() {
  // your code here...
}

function getMoreConnections() {
  // Optional: Depends on how you handle the "Load more"-Functionality
  // your code here...
}

function getConnectionsInCommon(userId, connectionId) {
  // your code here...
}

function getMoreConnectionsInCommon(userId, connectionId) {
  // Optional: Depends on how you handle the "Load more"-Functionality
  // your code here...
}

function getSuggestions(page) {
  // your code here...

    $.ajax({
        url: '/get-suggestions?page=' + page, // URL of the AJAX endpoint
        type: 'GET',
        dataType: 'json',
        success: function(response) {

            var receivedRequestsCount = response.receivedRequestsCount
            var sentRequestsCount = response.sentRequestsCount
            var userConnectionsCount = response.userConnectionsCount
            var suggestionsCount = response.suggestionsCount
            var suggestions = response.suggestions.data; // Retrieved users

            $('#suggestion_count').html(suggestionsCount);
            $('#sent_request_count').html(sentRequestsCount);
            $('#receive_request_count').html(receivedRequestsCount);
            $('#connection_count').html(userConnectionsCount);

            var html = '';
            if(suggestions){
              suggestions.forEach((suggestion, index) => {
                html += `<div class="d-flex justify-content-between my-2 shadow text-white bg-dark p-1" id="suggestion_list_id_${index}">
                  <table class="ms-1">
                    <td class="align-middle">
                      ${suggestion.name}
                    </td>
                    <td class="align-middle"> - </td>
                    <td class="align-middle">
                      ${suggestion.email}
                    </td>
                    <td class="align-middle"> 
                  </table>
                  <div>
                    <button onclick="sendRequest(${suggestion.id},'suggestion_list_id_${index}')" id="create_request_btn_" class="btn btn-primary me-1">Connect</button>
                  </div>
                </div>`;
              });
            }
            suggestionsList += html;
            // Update the suggestions list
            $('#suggestions_list').html(suggestionsList);
        }
    });
}


function getMoreSuggestions() {
  // Optional: Depends on how you handle the "Load more"-Functionality
  // your code here...
}

function sendRequest(userId, suggestionId) {
  // your code here...
}

function deleteRequest(userId, requestId) {
  // your code here...
}

function acceptRequest(userId, requestId) {
  // your code here...
}

function removeConnection(userId, connectionId) {
  // your code here...
}

function loadMoreSuggestions(){
  ++page;
  getSuggestions(page);
}

function loadMoreSentRequest(){
  ++page;
  getSentRequests(page);
}

function sendRequest(receiverId, suggestion_list_id){

  var data = {
    receiverId: receiverId
  };

  
  $.ajax({
    url: '/send-request',
    type: 'POST',
    data: data,
    success: function(response) {
      // Handle the success response
      console.log("response ====== ",response);

      var receivedRequestsCount = response.receivedRequestsCount
      var sentRequestsCount = response.sentRequestsCount
      var userConnectionsCount = response.userConnectionsCount
      var suggestionsCount = response.suggestionsCount

      $('#suggestion_count').html(suggestionsCount);
      $('#sent_request_count').html(sentRequestsCount);
      $('#receive_request_count').html(receivedRequestsCount);
      $('#connection_count').html(userConnectionsCount);

    },
    error: function(xhr, status, error) {
      // Handle the error response
    }
  });
  var divToRemove = document.getElementById(suggestion_list_id);
  divToRemove.remove();
}

function getSentRequests(page){

  $.ajax({
        url: '/get-sent-request?page=' + page, // URL of the AJAX endpoint
        type: 'GET',
        dataType: 'json',
        success: function(response) {

            var receivedRequestsCount = response.receivedRequestsCount
            var sentRequestsCount = response.sentRequestsCount
            var userConnectionsCount = response.userConnectionsCount
            var suggestionsCount = response.suggestionsCount
            var sentRequests = response.sentRequests.data; // Retrieved users

            $('#suggestion_count').html(suggestionsCount);
            $('#sent_request_count').html(sentRequestsCount);
            $('#receive_request_count').html(receivedRequestsCount);
            $('#connection_count').html(userConnectionsCount);

            var html = '';
            if(sentRequests){
              sentRequests.forEach((sent, index) => {
                html += `<div class="d-flex justify-content-between my-2 shadow text-white bg-dark p-1" id="sent_requests_id_${index}"
                        <table class="ms-1">
                          <td class="align-middle">
                            ${sent.receiver.name}
                          </td>
                          <td class="align-middle"> - </td>
                          <td class="align-middle">
                            ${sent.receiver.email}
                          </td>
                          <td class="align-middle">
                        </table>
                        <div>
                          <button id="cancel_request_btn_" class="btn btn-danger me-1" onclick="removeSentRequest(${sent.id}, 'sent_requests_id_${index}')">
                            Withdraw Request
                          </button>
                        </div>
                      </div>`;
              });
            }
            sentRequestList += html;
            // Update the suggestions list
            $('#sent_requests').html(sentRequestList);
        }
    });
}

function removeSentRequest(userConnectionId, sent_requests_id){
  console.log('userConnectionId ==== ', userConnectionId);

  $.ajax({
      url: '/remove-user-connection/' + userConnectionId,
      type: 'DELETE',
      success: function(response) {

        var receivedRequestsCount = response.receivedRequestsCount
        var sentRequestsCount = response.sentRequestsCount
        var userConnectionsCount = response.userConnectionsCount
        var suggestionsCount = response.suggestionsCount

        $('#suggestion_count').html(suggestionsCount);
        $('#sent_request_count').html(sentRequestsCount);
        $('#receive_request_count').html(receivedRequestsCount);
        $('#connection_count').html(userConnectionsCount);

        console.log('response === ', response);
      },
      error: function(xhr, status, error) {
      }
  });

  var divToRemove = document.getElementById(sent_requests_id);
  divToRemove.remove();

  
}

function getReceivedRequests(page){
  $.ajax({
        url: '/get-received-request?page=' + page, // URL of the AJAX endpoint
        type: 'GET',
        dataType: 'json',
        success: function(response) {

            var receivedRequestsCount = response.receivedRequestsCount
            var sentRequestsCount = response.sentRequestsCount
            var userConnectionsCount = response.userConnectionsCount
            var suggestionsCount = response.suggestionsCount
            var receivedRequests = response.receivedRequests.data; // Retrieved users

            $('#suggestion_count').html(suggestionsCount);
            $('#sent_request_count').html(sentRequestsCount);
            $('#receive_request_count').html(receivedRequestsCount);
            $('#connection_count').html(userConnectionsCount);

            var html = '';
            if(receivedRequests){
              receivedRequests.forEach((sent, index) => {
                html += `<div class="d-flex justify-content-between my-2 shadow text-white bg-dark p-1" id="received_requests_id_${index}"
                        <table class="ms-1">
                          <td class="align-middle">
                            ${sent.sender.name}
                          </td>
                          <td class="align-middle"> - </td>
                          <td class="align-middle">
                            ${sent.sender.email}
                          </td>
                          <td class="align-middle">
                        </table>
                        <div>
                          <button id="accept_request_btn_" class="btn btn-primary me-1" onclick="acceptReceivedRequest(${sent.id}, 'received_requests_id_${index}')">
                            Accept
                          </button>
                        </div>
                      </div>`;
              });
            }
            receivedRequestList += html;
            // Update the suggestions list
            $('#received_requests').html(receivedRequestList);
        }
    });
}


function acceptReceivedRequest(userConnectionId, received_requests_id){
  console.log('userConnectionId ==== ', userConnectionId);

  $.ajax({
      url: '/accept-received-request/' + userConnectionId,
      type: 'POST',
      success: function(response) {

        var receivedRequestsCount = response.receivedRequestsCount
        var sentRequestsCount = response.sentRequestsCount
        var userConnectionsCount = response.userConnectionsCount
        var suggestionsCount = response.suggestionsCount

        $('#suggestion_count').html(suggestionsCount);
        $('#sent_request_count').html(sentRequestsCount);
        $('#receive_request_count').html(receivedRequestsCount);
        $('#connection_count').html(userConnectionsCount);

        console.log('response === ', response);
      },
      error: function(xhr, status, error) {
      }
  });

  var divToRemove = document.getElementById(received_requests_id);
  divToRemove.remove();

}

function getConnections(page){
   $.ajax({
        url: '/get-connections?page=' + page, // URL of the AJAX endpoint
        type: 'GET',
        dataType: 'json',
        success: function(response) {

            var receivedRequestsCount = response.receivedRequestsCount
            var sentRequestsCount = response.sentRequestsCount
            var userConnectionsCount = response.userConnectionsCount
            var suggestionsCount = response.suggestionsCount
            var connections = response.connections.data; // Retrieved users

            $('#suggestion_count').html(suggestionsCount);
            $('#sent_request_count').html(sentRequestsCount);
            $('#receive_request_count').html(receivedRequestsCount);
            $('#connection_count').html(userConnectionsCount);

            var html = '';
            if(connections){
              connections.forEach((sent, index) => {
                html += `<div class="d-flex justify-content-between my-2 shadow text-white bg-dark p-1" id="received_requests_id_${index}"
                        <table class="ms-1">
                          <td class="align-middle">
                            ${sent.sender.name}
                          </td>
                          <td class="align-middle"> - </td>
                          <td class="align-middle">
                            ${sent.sender.email}
                          </td>
                          <td class="align-middle">
                        </table>
                        <div>
                          <button id="accept_request_btn_" class="btn btn-primary me-1" onclick="acceptReceivedRequest(${sent.id}, 'received_requests_id_${index}')">
                            Accept
                          </button>
                        </div>
                      </div>`;
              });
            }
            connectionsList += html;
            // Update the suggestions list
            $('#received_requests').html(connectionsList);
        }
    });

  
}

function changeTab(value){
    $('.tabs-list-div').css('display', 'none'); // Remove the CSS style from all tabs
    $(`#${value}`).css('display', 'block'); // Apply the CSS style to the clicked tab

    if(value == 'sent_requests_div'){
      if(page != 1){
        ++page;
      }else{
        sentRequestList = [];
      }
      getSentRequests(page);
    }else if(value == 'receive_requests_div'){
      if(page != 1){
        ++page;
      }else{
        receivedRequestList = [];
      }
      getReceivedRequests(page);
    }
}
$(function () {
  //getSuggestions();
  getSuggestions(page);
});